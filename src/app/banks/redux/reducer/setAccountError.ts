import { BanksState } from '@/app/banks/redux/state';

export default function setAccountError(
  state: BanksState,
  accountId: string | undefined,
  errorMsg: string | undefined,
): BanksState {
  if (!accountId) {
    return { ...state, errorMsg: 'Account Id missing.' };
  }
  const account = { ...state.accounts[accountId] };
  if (!Object.keys(account).length) {
    return { ...state, errorMsg: `Account ${accountId} could not be found.` };
  }
  const newState: BanksState = { ...state };
  account.errorMsg = errorMsg;
  newState.accounts = { ...newState.accounts, [accountId]: account };
  return newState;
}

/* eslint-disable import/prefer-default-export */
import { RootState } from '@/app/base/redux';
import BankAccountState from '@/app/banks/interfaces/BankAccountState';

export const getAccounts = (store: RootState):
  {[key: string]: BankAccountState} => store.banks.accounts;

export const getIsAccountsReloading = (store: RootState):
  boolean => store.banks.isReloading;

export const getAccountsErrorMsg = (store: RootState):
  string | undefined => store.banks.errorMsg;

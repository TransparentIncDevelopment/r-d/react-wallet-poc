import {
  BanksApi,
  Configuration,
  BankAccount,
  BankAccountBalance,
} from 'xand-member-api-client';

import routes from '@/app/base/constants/routes';

const apiConfig = new Configuration({
  basePath: routes.api,
});

const banksApi = new BanksApi(apiConfig);

export const getAccounts = async ():
  Promise<BankAccount[]> => (await banksApi.getBankAccounts()).data;

export const getAccountBalance = async (accountId: string):
  Promise<BankAccountBalance> => (await banksApi.getBankAccountBalance(accountId)).data;

export const getAccount = async (accountId: string):
  Promise<BankAccount> => (await banksApi.getBankAccountById(accountId)).data;

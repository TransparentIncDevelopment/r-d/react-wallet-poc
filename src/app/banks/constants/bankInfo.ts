import OctopodaBankLogo from '@/images/OctopodaBank.svg';
import ChamberedTrustLogo from '@/images/ChamberedTrust.svg';
import TentacleFinancialLogo from '@/images/TentacleFinancial.svg';
import BankInfo from '@/app/banks/interfaces/BankInfo';

const bankLookupTable: Array<[string, BankInfo]> = [
  ['928173892', { name: 'Chambered Trust', logo: ChamberedTrustLogo }],
  ['121141343', { name: 'Octopoda Bank', logo: OctopodaBankLogo }],
  ['211374020', { name: 'Tentacle Financial', logo: TentacleFinancialLogo }],
];

export default bankLookupTable;

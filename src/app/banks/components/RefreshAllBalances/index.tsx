/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import Button from '@material-ui/core/Button';
import { useDispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';

import { refreshAllAccounts } from '@/app/banks/redux/actions';

export default function RefreshAllBalances(): JSX.Element {
  const dispatch = useDispatch();

  return (
    <Button
      variant="contained"
      component="label"
      color="primary"
      onClick={(): ThunkAction<any, any, any, any> => dispatch(refreshAllAccounts())}
    >
      Refresh All Accounts
    </Button>
  );
}

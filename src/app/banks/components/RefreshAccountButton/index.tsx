/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import Button from '@material-ui/core/Button';
import { useDispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';

import { refreshAccountBalance } from '@/app/banks/redux/actions';

export interface RefreshAccountButtonProps {
  accountId: string;
}

export default function RefreshAccountButton(props: RefreshAccountButtonProps): JSX.Element {
  const { accountId } = props;
  const dispatch = useDispatch();

  return (
    <Button
      size="small"
      variant="contained"
      component="label"
      color="secondary"
      onClick={(): ThunkAction<any, any, any, any> => dispatch(refreshAccountBalance(accountId))}
    >
      Refresh Account
    </Button>
  );
}

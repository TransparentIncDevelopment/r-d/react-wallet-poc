import React from 'react';
import { Grid } from '@material-ui/core';

import AccountListItem from '@/app/banks/components/AccountListItem';
import BankAccountState from '@/app/banks/interfaces/BankAccountState';

export interface AccountListProps {
  accounts: {[key: string]: BankAccountState};
}

function buildAccountList(accounts: {[key: string]: BankAccountState}): JSX.Element[] {
  const accts = Object.values(accounts);
  return accts.map((acct: BankAccountState):
    JSX.Element => <AccountListItem key={acct.accountId} account={acct} />);
}

export default function AccountList(props: AccountListProps): JSX.Element {
  const { accounts } = props;
  const accountList = accounts && buildAccountList(accounts);
  return (
    <Grid
      className="account-list-root"
      container
      direction="column"
    >
      { accountList }
    </Grid>
  );
}

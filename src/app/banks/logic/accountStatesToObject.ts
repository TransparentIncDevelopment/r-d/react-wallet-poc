import BankAccountState from '@/app/banks/interfaces/BankAccountState';

const accountStatesToObject = (accounts: BankAccountState[]): {[key: string]: BankAccountState} => {
  const obj = {};
  accounts.forEach((acct: BankAccountState) => {
    if (acct.accountId) {
      obj[acct.accountId] = acct;
    }
  });
  return obj;
};

export default accountStatesToObject;

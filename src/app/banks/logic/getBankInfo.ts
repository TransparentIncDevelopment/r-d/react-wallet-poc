import bankInfoTable from '@/app/banks/constants/bankInfo';
import BankInfo from '@/app/banks/interfaces/BankInfo';

const bankLookup: Map<string, BankInfo> = new Map(bankInfoTable);
const getBankInfo = (routingNum: string): BankInfo | undefined => bankLookup.get(routingNum);

export default getBankInfo;

import React from 'react';
import Button from '@material-ui/core/Button';
import { useDispatch } from 'react-redux';

import { logout, UserAction } from '@/app/user/redux/actions';


export default function Logout(): JSX.Element {
  const dispatch = useDispatch();

  return (
    <Button
      variant="contained"
      component="label"
      color="secondary"
      onClick={(): UserAction => dispatch((logout()))}
    >
      Logout
    </Button>
  );
}

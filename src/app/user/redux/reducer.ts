import persistence from '@/app/base/logic/persistence';
import UserActionType from './types';
import { UserAction } from './actions';
import { loadInitialUserState, UserState } from './state';

const initialState = loadInitialUserState();

const userReducer = (state: UserState = initialState, action: UserAction): UserState => {
  switch (action.type) {
    case UserActionType.Logout: {
      persistence.setAddress(null);
      return loadInitialUserState();
    }
    case UserActionType.SetAddress: {
      persistence.setAddress(action.payload || '');
      return { ...state, address: action.payload || '' };
    }
    case UserActionType.SetJwtToken: {
      persistence.setJwtToken(state.address, action.payload);
      return { ...state, jwtToken: action.payload || '' };
    }
    default: {
      return state;
    }
  }
};

export default userReducer;

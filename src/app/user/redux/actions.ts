import UserActionTypes from './types';

export interface UserAction {
  type: UserActionTypes;
  payload?: string;
}

export const logout = (): UserAction => ({
  type: UserActionTypes.Logout,
});


export const setUserAddress = (address: string): UserAction => ({
  type: UserActionTypes.SetAddress,
  payload: address,
});

export const setJwtToken = (token: string): UserAction => ({
  type: UserActionTypes.SetJwtToken,
  payload: token,
});

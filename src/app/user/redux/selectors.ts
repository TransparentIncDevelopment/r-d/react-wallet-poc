/* eslint-disable max-len */
import { RootState } from '@/app/base/redux';

export const getUserAddress = (store: RootState): string => store && store.user && store.user.address;
export const getJwtToken = (store: RootState): string => store && store.user && store.user.jwtToken;

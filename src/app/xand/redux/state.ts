import Money from 'tpfs-money';

export interface XandState {
  walletBalance: Money;
  loadingBalance: boolean;
  balanceError: string | undefined;
}

export const initialState: XandState = {
  walletBalance: new Money(0, 'USD'),
  loadingBalance: false,
  balanceError: undefined,
};

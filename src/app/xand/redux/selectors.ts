/* eslint-disable import/prefer-default-export */
import Money from 'tpfs-money';
import { RootState } from '@/app/base/redux';

export const getWalletBalance = (store: RootState): Money => store.xand.walletBalance;
export const getIsWalletLoading = (store: RootState): boolean => store.xand.loadingBalance;
export const getWalletErr = (store: RootState): string | undefined => store.xand.balanceError;

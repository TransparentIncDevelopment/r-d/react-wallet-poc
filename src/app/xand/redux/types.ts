enum XandActionTypes {
  SetBalance = 'set_wallet_balance',
  SetBalanceLoading = 'set_balance_loading',
  SetBalanceError = 'set_balance_error',
}

export default XandActionTypes;

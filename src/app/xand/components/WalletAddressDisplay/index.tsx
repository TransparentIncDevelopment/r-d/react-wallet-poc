import React from 'react';
import { Typography, Button } from '@material-ui/core';
import CopyToClipboard from 'react-copy-to-clipboard';

export interface WalletAddressDisplayProps {
  address: string;
  size: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'body1' | 'body2';
}

export default function WalletAddressDisplay(props: WalletAddressDisplayProps): JSX.Element {
  const { address, size } = props;

  return (
    <CopyToClipboard text={address}>
      <Button color="primary">
        <Typography variant={size}>
          {`${address}`}
        </Typography>
      </Button>
    </CopyToClipboard>
  );
}

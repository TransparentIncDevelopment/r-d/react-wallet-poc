/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import Button from '@material-ui/core/Button';
import { useSelector, useDispatch } from 'react-redux';
import { ThunkAction } from 'redux-thunk';

import { getUserAddress } from '@/app/user/redux/selectors';
import { refreshBalance } from '@/app/xand/redux/actions';

export default function RefreshWalletBalance(): JSX.Element {
  const address = useSelector(getUserAddress);
  const dispatch = useDispatch();

  return (
    <Button
      variant="contained"
      component="label"
      color="primary"
      onClick={(): ThunkAction<any, any, any, any> => dispatch(refreshBalance(address))}
    >
      Refresh Wallet Balance
    </Button>
  );
}

import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';

import { getUserAddress } from '@/app/user/redux/selectors';
import { getWalletBalance, getIsWalletLoading, getWalletErr } from '@/app/xand/redux/selectors';
import WalletBalanceDisplay from '@/app/xand/components/WalletBalanceDisplay';
import WalletAddressDisplay from '@/app/xand/components/WalletAddressDisplay';
import RefreshWalletBalance from '@/app/xand/components/RefreshWalletBalance';

import './style.scss';

export default function WalletInfoSection(): JSX.Element {
  const userAddress = useSelector(getUserAddress);
  const balance = useSelector(getWalletBalance);
  const isWalletLoading = useSelector(getIsWalletLoading);
  const walletErr = useSelector(getWalletErr);

  return (
    <Grid
      className="wallet-info-section-root wallet-info wallet-info-section"
      container
      direction="column"
    >
      <Grid item>
        <Typography variant="h5" className="heading">Wallet Information</Typography>
      </Grid>
      <Grid item>
        <WalletAddressDisplay size="body1" address={userAddress} />
      </Grid>
      <Grid item>
        <WalletBalanceDisplay errorMsg={walletErr} isRefreshing={isWalletLoading} size="h2" balance={balance} />
      </Grid>
      <Grid item>
        <div className="options-container">
          <RefreshWalletBalance />
        </div>
      </Grid>
    </Grid>
  );
}

/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducer';

const middlewareEnhancer = applyMiddleware(thunk);
const chromeDevTool = (window as any).__REDUX_DEVTOOLS_EXTENSION__;
const enhancers = compose(middlewareEnhancer, chromeDevTool()); // Add chromDevTool() here to enable chrome redux debugger

export default createStore(rootReducer, undefined, enhancers);

export type RootState = ReturnType<typeof rootReducer>;

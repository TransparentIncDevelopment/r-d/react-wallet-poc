import { combineReducers } from 'redux';

import userReducer from '@/app/user/redux/reducer';
import xandReducer from '@/app/xand/redux/reducer';
import banksReducer from '@/app/banks/redux/reducer';

export default combineReducers({
  user: userReducer,
  xand: xandReducer,
  banks: banksReducer,
});

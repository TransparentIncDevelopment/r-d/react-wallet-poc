export default {
  home: '/',
  transfer: '/transfer',
  history: '/history',
  login: '/login',
  receipt: '/receipt',
  settings: '/settings',
  api: '/api',
};

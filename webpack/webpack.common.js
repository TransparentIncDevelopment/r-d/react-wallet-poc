const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const WebpackBundleAnalyzer = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')

const isDevEnv = require('./isDevEnv');
const paths = require('./paths');

module.exports = {
    context: paths.src,
    entry: [path.resolve(paths.src, "App.tsx")],
    target: 'web',
    resolve: {
        alias: {
            '@': paths.src,
        },
        extensions: ['.js', '.ts', '.jsx', '.tsx']
    },
    plugins: [
        // https://www.npmjs.com/package/webpack-bundle-analyzer
        new WebpackBundleAnalyzer({
            analyzerMode: 'static',
            reportFilename: isDevEnv ?
                path.resolve(paths.reports, 'webpack-analysis-dev.html') :
                path.resolve(paths.reports, 'webpack-analysis-prod.html'),
            openAnalyzer: false,
        }),
        //https://webpack.js.org/guides/output-management/#cleaning-up-the-dist-folder
        new CleanWebpackPlugin(),
        // https://github.com/jantimon/html-webpack-plugin
        new HtmlWebpackPlugin({
            template: path.join(paths.src, 'index.html'),
            hash: true,
            chunksSortMode: 'auto'
        }),
        // https://www.npmjs.com/package/favicons-webpack-plugin#html-injection
        new FaviconsWebpackPlugin(path.resolve(paths.src, 'images/transparent_logo.svg')),
        new FriendlyErrorsWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.(js|ts|jsx|tsx)$/,
                include: [paths.src],
                loader: "babel-loader"
            },
            {
                test: /\.(png|jpg|jpeg|gif|ttf|otf|svg|pdf)$/,
                loader: "file-loader"
            },
            {
                test: /\.css$/,
                use: [
                    isDevEnv ? "style-loader" : MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDevEnv } },
                ]
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    isDevEnv ? 'style-loader' : MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDevEnv } },
                    { loader: 'sass-loader', options: { sourceMap: isDevEnv, prependData: '@import \'@/style/_variables.scss\';' } }
                ]
            },
        ]
    },

}